package com.pincello.campanhas.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.pincello.campanhas.SistemaCampanhasApplicationTests;
import com.pincello.campanhas.controller.CampanhaController;
import com.pincello.campanhas.domain.Campanha;
import com.pincello.campanhas.exception.CampanhaInvalidaException;
import com.pincello.campanhas.helper.CampanhaFake;
import com.pincello.campanhas.helper.JsonHelper;
import com.pincello.campanhas.service.CampanhaService;

/**
 * Testes do controller responsável pela exposição das APIs de Campanha
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class CampanhaControllerTest extends SistemaCampanhasApplicationTests {
	
	private MockMvc mockMvc;
	
	@Autowired
	CampanhaController campanhaController;
	
	@Autowired
	CampanhaService campanhaService;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(campanhaController).build();
	}
	
	@After
	public void tearDown() {
	  JdbcTestUtils.deleteFromTables(jdbcTemplate, "campanha");
	}
	
	@Test
	public void testGETListNoContent() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas")).andExpect(MockMvcResultMatchers.status().isNoContent());
	}
	
	@Test
	public void testGETList() throws Exception {
		createCampanha(CampanhaFake.create());
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testGETListByTimeNoContent() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas/time/1")).andExpect(MockMvcResultMatchers.status().isNoContent());
	}
	
	@Test
	public void testGETListByTime() throws Exception {
		Campanha campanha = createCampanha(CampanhaFake.create());
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas/time/" + campanha.getIdTimeCoracao())).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testGETCampanhaNotFound() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas/1")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void testGETCampanha() throws Exception {
		Campanha campanha = createCampanha(CampanhaFake.create());
		this.mockMvc.perform(MockMvcRequestBuilders.get("/campanhas/" + campanha.getId())).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testPOSTCampanhaBadRequestNomeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setNome(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTCampanhaBadRequestTimeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setIdTimeCoracao(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTCampanhaBadRequestDataInicioInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataInicio(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTCampanhaBadRequestDataTerminoInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataTermino(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTCampanhaBadRequestVigenciaInvalida() throws Exception {
		Campanha campanha = CampanhaFake.createWithVigenciaInvalida();
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTCampanha() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.post("/campanhas")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(CampanhaFake.create()))
				).andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	@Test
	public void testPUTCampanhaNotFound() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.put("/campanhas/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(CampanhaFake.create()))
				).andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void testPUTCampanha() throws Exception {
		Campanha campanha = createCampanha(CampanhaFake.create());
		this.mockMvc.perform(MockMvcRequestBuilders.put("/campanhas/" + campanha.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(campanha))
				).andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	@Test
	public void testDELETECampanhaNotFound() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.delete("/campanhas/1")).andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void testDELETECampanha() throws Exception {
		Campanha campanha = createCampanha(CampanhaFake.create());
		this.mockMvc.perform(MockMvcRequestBuilders.delete("/campanhas/" + campanha.getId())).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	private Campanha createCampanha(Campanha campanha) throws CampanhaInvalidaException {
		return campanhaService.createCampanha(campanha);
	}

}
