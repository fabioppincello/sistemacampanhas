package com.pincello.campanhas.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.github.javafaker.Faker;
import com.pincello.campanhas.SistemaCampanhasApplicationTests;
import com.pincello.campanhas.domain.Campanha;
import com.pincello.campanhas.exception.CampanhaInvalidaException;
import com.pincello.campanhas.helper.CampanhaFake;
import com.pincello.campanhas.persistence.CampanhasRepository;
import com.pincello.campanhas.persistence.entity.CampanhaEntity;
import com.pincello.campanhas.service.CampanhaService;

/**
 * Testes da classe que implementa as regras de negócio referente às campanhas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class CampanhaServiceTest extends SistemaCampanhasApplicationTests {
	
	@Autowired
	CampanhaService campanhaService;
	
	@Autowired
	private CampanhasRepository campanhasRepository;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@After
	public void tearDown() {
	  JdbcTestUtils.deleteFromTables(jdbcTemplate, "campanha");
	}
	
	@Test
	public void testGetCampanhasEmpty() throws Exception {
		createCampanha(CampanhaFake.createVencida());
		List<Campanha> campanhas = campanhaService.getCampanhasNaoVencidas();
		assertTrue(campanhas.isEmpty());
	}
	
	@Test
	public void testGetCampanhas() throws Exception {
		createCampanha(CampanhaFake.create());
		List<Campanha> campanhas = campanhaService.getCampanhasNaoVencidas();
		assertFalse(campanhas.isEmpty());
	}
	
	@Test
	public void testGetCampanhasTimeEmpty() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.create());
		List<Campanha> campanhas = campanhaService.getCampanhasTime(campanhaEntity.getIdTimeCoracao() + 1);
		assertTrue(campanhas.isEmpty());
	}
	
	@Test
	public void testGetCampanhasTime() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.create());
		List<Campanha> campanhas = campanhaService.getCampanhasTime(campanhaEntity.getIdTimeCoracao());
		assertFalse(campanhas.isEmpty());
	}
	
	@Test
	public void testGetCampanhaNull() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.createVencida());
		Campanha campanha = campanhaService.getCampanhaNaoVencida(campanhaEntity.getId());
		assertNull(campanha);
	}
	
	@Test
	public void testGetCampanha() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.create());
		Campanha campanha = campanhaService.getCampanhaNaoVencida(campanhaEntity.getId());
		assertNotNull(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testCreateCampanhaNomeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setNome(null);
		campanhaService.createCampanha(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testCreateCampanhaTimeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setIdTimeCoracao(null);
		campanhaService.createCampanha(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testCreateCampanhaDataInicioInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataInicio(null);
		campanhaService.createCampanha(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testCreateCampanhaDataTerminoInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataTermino(null);
		campanhaService.createCampanha(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testCreateCampanhaVigenciaInvalida() throws Exception {
		Campanha campanha = CampanhaFake.createWithVigenciaInvalida();
		campanhaService.createCampanha(campanha);
	}
	
	@Test
	public void testCreateCampanha() throws Exception {
		Campanha campanha = campanhaService.createCampanha(CampanhaFake.create());
		assertNotNull(campanha);
		assertTrue(campanhasRepository.existsById(campanha.getId()));
	}
	
	@Test
	public void testCreateMultipleCampanhas() throws Exception {
		Campanha campanhaFake = CampanhaFake.create();
		for(int i = 0; i < 10; i++) {
			campanhaService.createCampanha(campanhaFake);
		}
		List<CampanhaEntity> campanhasEntity = campanhasRepository.findAll();
		assertTrue(campanhasEntity != null && !campanhasEntity.isEmpty() && campanhasEntity.size() == 10);
		List<Campanha> campanhas = new ArrayList<Campanha>();
		for(CampanhaEntity campanhaEntity : campanhasEntity) {
			if(campanhaEntity != null) {
				campanhas.add(modelMapper.map(campanhaEntity, Campanha.class));
			}
		}
		Collections.sort(campanhas);
		Date nextDate = null;
		for(Campanha campanha : campanhas) {
			if(nextDate != null) {
				assertTrue(campanha.getDataTermino().after(nextDate));
			}
			nextDate = campanha.getDataTermino();
		}
	}
	
	@Test
	public void testUpdateCampanhaInexistente() throws Exception {
		Campanha campanha = campanhaService.updateCampanha(new Faker().random().nextLong(), CampanhaFake.create());
		assertNull(campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testUpdateCampanhaNomeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setNome(null);
		campanhaService.updateCampanha(campanha.getId(), campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testUpdateCampanhaTimeInvalido() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setIdTimeCoracao(null);
		campanhaService.updateCampanha(campanha.getId(), campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testUpdateCampanhaDataInicioInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataInicio(null);
		campanhaService.updateCampanha(campanha.getId(), campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testUpdateCampanhaDataTerminoInvalida() throws Exception {
		Campanha campanha = CampanhaFake.create();
		campanha.setDataTermino(null);
		campanhaService.updateCampanha(campanha.getId(), campanha);
	}
	
	@Test(expected = CampanhaInvalidaException.class)
	public void testUpdateCampanhaVigenciaInvalida() throws Exception {
		Campanha campanha = CampanhaFake.createWithVigenciaInvalida();
		campanhaService.updateCampanha(campanha.getId(), campanha);
	}
	
	@Test
	public void testUpdateCampanha() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.create());
		Campanha campanha = campanhaService.updateCampanha(campanhaEntity.getId(), CampanhaFake.create());
		assertNotNull(campanha);
	}
	
	@Test
	public void testNotDeleteCampanha() throws Exception {
		boolean deleted = campanhaService.deleteCampanha(new Faker().random().nextLong());
		assertFalse(deleted);
	}
	
	@Test
	public void testDeleteCampanha() throws Exception {
		CampanhaEntity campanhaEntity = createCampanha(CampanhaFake.create());
		boolean deleted = campanhaService.deleteCampanha(campanhaEntity.getId());
		assertTrue(deleted);
	}
	
	private CampanhaEntity createCampanha(Campanha campanha) throws Exception {
		CampanhaEntity campanhaEntity = new CampanhaEntity();
		campanhaEntity.setVersao(1);
		campanhaEntity.setNome(campanha.getNome());
		campanhaEntity.setIdTimeCoracao(campanha.getIdTimeCoracao());
		campanhaEntity.setDataInicio(campanha.getDataInicio());
		campanhaEntity.setDataTermino(campanha.getDataTermino());
		return campanhasRepository.save(campanhaEntity);
	}

}
