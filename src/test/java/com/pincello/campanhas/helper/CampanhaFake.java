package com.pincello.campanhas.helper;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.github.javafaker.Faker;
import com.pincello.campanhas.domain.Campanha;
import com.pincello.campanhas.helper.DateHelper;

/**
 * Helper para geração de objetos de campanhas aleatórias
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class CampanhaFake {
	
	public static Campanha create() {
		Faker faker = new Faker();
		return create(faker.date().future(10, TimeUnit.DAYS), faker.random().nextInt(1, 12));
	}
	
	public static Campanha createVencida() {
		Faker faker = new Faker();
		return create(faker.date().past(10, TimeUnit.DAYS), faker.random().nextInt(1, 12));
	}
	
	public static Campanha createWithVigenciaInvalida() {
		Campanha campanha = create();
		campanha.setDataInicio(DateHelper.addDays(campanha.getDataTermino(), 1));
		return campanha;
	}
	
	private static Campanha create(Date dataTermino, Integer idTimeCoracao) {
		Faker faker = new Faker();
		Campanha campanha = new Campanha();
		campanha.setNome(faker.commerce().productName());
		campanha.setIdTimeCoracao(idTimeCoracao);
		campanha.setDataInicio(faker.date().past(20, TimeUnit.DAYS));
		campanha.setDataTermino(dataTermino);
		return campanha;
	}

}
