package com.pincello.campanhas.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pincello.campanhas.domain.Campanha;
import com.pincello.campanhas.domain.Response;
import com.pincello.campanhas.enums.MensagemEnum;
import com.pincello.campanhas.exception.CampanhaInvalidaException;
import com.pincello.campanhas.helper.HttpErrorHelper;
import com.pincello.campanhas.service.CampanhaService;

/**
 * Controller responsável pela exposição das APIs de Campanha
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@RestController
@RequestMapping("/campanhas")
public class CampanhaController {
	
	private static final Logger logger = LogManager.getLogger(CampanhaController.class);
	
	@Autowired
	private CampanhaService campanhaService;
	
	/**
	 * Método utilizado para expor a busca de todas as campanhas
	 * com data de vigência não vencida
	 * 
	 * @return
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<Campanha>>> getCampanhas() {
		logger.trace("getCampanhas()");
		
		Response<List<Campanha>> response = new Response<List<Campanha>>();
		
		List<Campanha> campanhasResponse = null;
		try {
			campanhasResponse = campanhaService.getCampanhasNaoVencidas();
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		if(campanhasResponse == null || campanhasResponse.isEmpty()) {
			return new ResponseEntity<Response<List<Campanha>>>(HttpStatus.NO_CONTENT);
		}
		
		response.setData(campanhasResponse);
		
		return new ResponseEntity<Response<List<Campanha>>>(response, HttpStatus.OK);
	}
	
	/**
	 * Método utilizado para expor a busca de uma campanha pelo id,
	 * desde que não esteja com data de vigência vencida
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/time/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<List<Campanha>>> getCampanhasTime(@PathVariable("id") Integer id) {
		logger.trace(String.format("getCampanhasTime(%s)", id));
		
		Response<List<Campanha>> response = new Response<List<Campanha>>();
		
		List<Campanha> campanhasResponse = null;
		try {
			campanhasResponse = campanhaService.getCampanhasTime(id);
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		if(campanhasResponse == null || campanhasResponse.isEmpty()) {
			return new ResponseEntity<Response<List<Campanha>>>(HttpStatus.NO_CONTENT);
		}
		
		response.setData(campanhasResponse);
		
		return new ResponseEntity<Response<List<Campanha>>>(response, HttpStatus.OK);
	}
	
	/**
	 * Método utilizado para expor a busca de todas as campanhas de um
	 * determinado time, desde que não esteja com data de vigência vencida
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Campanha>> getCampanha(@PathVariable("id") Long id) {
		logger.trace(String.format("getCampanha(%s)", id));
		
		Response<Campanha> response = new Response<Campanha>();
		
		Campanha campanhaResponse = null;
		try {
			campanhaResponse = campanhaService.getCampanhaNaoVencida(id);
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		if(campanhaResponse == null) {
			return HttpErrorHelper.getNotFoundResponse(MensagemEnum.MSG_CAMPANHA_NAO_ENCONTRADA, id);
		}
		
		response.setData(campanhaResponse);
		
		return new ResponseEntity<Response<Campanha>>(response, HttpStatus.OK);
	}
	
	/**
	 * Método utilizado para expor o cadastro de uma nova campanha
	 * 
	 * @param campanha
	 * @param result
	 * @return
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Campanha>> createCampanha(@Valid @RequestBody Campanha campanha, BindingResult result) {
		logger.trace(String.format("createCampanha(%s)", campanha));
		
		Response<Campanha> response = new Response<Campanha>();
		response.setErrors(result);
		
		if(result.hasErrors()) {
			return new ResponseEntity<Response<Campanha>>(response, HttpStatus.BAD_REQUEST);
		}
		
		Campanha campanhaResponse = null;
		try {
			campanhaResponse = campanhaService.createCampanha(campanha);
		} catch(CampanhaInvalidaException e) {
			response.getErrors().add(e.getMessage());
			return new ResponseEntity<Response<Campanha>>(response, HttpStatus.BAD_REQUEST);
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		response.setData(campanhaResponse);
		
		return new ResponseEntity<Response<Campanha>>(response, HttpStatus.CREATED);
	}
	
	/**
	 * Método utilizado para expor a atualização de uma campanha já existente.
	 * A campanha é identificada pelo id.
	 * 
	 * @param id
	 * @param campanha
	 * @param result
	 * @return
	 */
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<Campanha>> updateCampanha(@PathVariable("id") Long id, @Valid @RequestBody Campanha campanha, BindingResult result) {
		logger.trace(String.format("updateCampanha(%s, %s)", id, campanha));
		
		Response<Campanha> response = new Response<Campanha>();
		response.setErrors(result);
		
		if(result.hasErrors()) {
			return new ResponseEntity<Response<Campanha>>(response, HttpStatus.BAD_REQUEST);
		}
		
		Campanha campanhaResponse = null;
		try {
			campanhaResponse = campanhaService.updateCampanha(id, campanha);
		} catch(CampanhaInvalidaException e) {
			response.getErrors().add(e.getMessage());
			return new ResponseEntity<Response<Campanha>>(response, HttpStatus.BAD_REQUEST);
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		if(campanhaResponse == null) {
			return HttpErrorHelper.getNotFoundResponse(MensagemEnum.MSG_CAMPANHA_NAO_ENCONTRADA, id);
		}
		
		response.setData(campanhaResponse);
		
		return new ResponseEntity<Response<Campanha>>(response, HttpStatus.CREATED);
	}
	
	/**
	 * Método utilizado para expor a exclusão de uma campanha.
	 * A campanha é identificada pelo id.
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteCampanha(@PathVariable("id") Long id) {
		logger.trace(String.format("deleteCampanha(%s)", id));
		
		Response<String> response = new Response<String>();
		
		boolean deleted = false;
		try {
			deleted = campanhaService.deleteCampanha(id);
		} catch(Exception e) {
			return HttpErrorHelper.getErrorResponse(e);
		}
		
		if (deleted) {
			response.setData(MensagemEnum.MSG_CAMPANHA_DELETADA.getMensagem(id));
			return new ResponseEntity<Response<String>>(response, HttpStatus.OK);
		}
		
		return HttpErrorHelper.getNotFoundResponse(MensagemEnum.MSG_CAMPANHA_NAO_ENCONTRADA, id);
	}

}
