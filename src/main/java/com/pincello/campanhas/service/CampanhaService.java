package com.pincello.campanhas.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pincello.campanhas.domain.Campanha;
import com.pincello.campanhas.enums.MensagemEnum;
import com.pincello.campanhas.exception.CampanhaInvalidaException;
import com.pincello.campanhas.helper.DateHelper;
import com.pincello.campanhas.persistence.CampanhasRepository;
import com.pincello.campanhas.persistence.entity.CampanhaEntity;

/**
 * Classe que implementa as regras de negócio referente às campanhas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Service
public class CampanhaService {
	
	private static final Logger logger = LogManager.getLogger(CampanhaService.class);
	
	@Autowired
	CampanhasRepository campanhasRepository;
	
	@Autowired
    private ModelMapper modelMapper;
	
	private Date dataComparacao;
	
	/**
	 * Método responsável por obter as campanhas que não estão com a data de
	 * vigência vencida.
	 * 
	 * @return
	 */
	@Transactional
	public List<Campanha> getCampanhasNaoVencidas() {
		logger.trace("getCampanhasNaoVencidas()");
		
		List<CampanhaEntity> campanhasEntity = campanhasRepository.findByDataTerminoGreaterThanEqual(new Date());
		List<Campanha> campanhas = convertToDomain(campanhasEntity);
		
		return campanhas;
	}
	
	/**
	 * Método responsável por obter as campanhas de um determinado time, desde
	 * que não estejam com a data de vigência vencida.
	 * 
	 * @param idTimeCoracao
	 * @return
	 */
	@Transactional
	public List<Campanha> getCampanhasTime(Integer idTimeCoracao) {
		logger.trace(String.format("getCampanhasTime(%s)", idTimeCoracao));
		
		List<CampanhaEntity> campanhasEntity = campanhasRepository.findByIdTimeCoracaoAndDataTerminoGreaterThanEqual(idTimeCoracao, new Date());
		List<Campanha> campanhas = convertToDomain(campanhasEntity);
		
		return campanhas;
	}
	
	/**
	 * Método responsável por obter uma campanha pelo id, desde
	 * que não esteja com a data de vigência vencida.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public Campanha getCampanhaNaoVencida(Long id) {
		logger.trace(String.format("getCampanhaNaoVencida(%s)", id));
		
		if(campanhasRepository.existsById(id)) {
			return convertToDomain(campanhasRepository.findByIdAndDataTerminoGreaterThanEqual(id, new Date()));
		}
		
		return null;
	}
	
	/**
	 * Método responsável por cadastrar uma nova campanha.
	 * 
	 * @param campanha
	 * @return
	 * @throws CampanhaInvalidaException
	 */
	@Transactional
	public Campanha createCampanha(Campanha campanha) throws CampanhaInvalidaException {
		logger.trace(String.format("createCampanha(%s)", campanha));
		
		validarCampanha(campanha);
		
		List<Campanha> campanhasAtivas = getCampanhasAtivasNoPeriodo(campanha);
		if(campanhasAtivas != null && !campanhasAtivas.isEmpty()) {
			atualizarVigencias(campanhasAtivas, campanha.getDataTermino());
		}
		
		campanha = create(campanha);
		return campanha;
	}
	
	/**
	 * Método responsável por atualizar uma campanha existente.
	 * A campanha é identificada pelo id.
	 * 
	 * @param id
	 * @param campanha
	 * @return
	 * @throws CampanhaInvalidaException
	 */
	@Transactional
	public Campanha updateCampanha(Long id, Campanha campanha) throws CampanhaInvalidaException {
		logger.trace(String.format("updateCampanha($s, %s)", id, campanha));
		
		validarCampanha(campanha);
		
		if(campanhasRepository.existsById(id)) {
			
			List<Campanha> campanhasAtivas = getCampanhasAtivasNoPeriodo(campanha);
			if(campanhasAtivas != null && !campanhasAtivas.isEmpty()) {
				atualizarVigencias(campanhasAtivas, campanha.getDataTermino());
			}
			
			CampanhaEntity campanhaEntity = campanhasRepository.getOne(id);
			campanha.setId(id);
			campanha.setVersao(campanhaEntity.getVersao());
			campanha = update(campanha);
			return campanha;
		}
		return null;
	}
	
	/**
	 * Método responsável por excluir uma campanha.
	 * A campanha é identificada pelo id.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public boolean deleteCampanha(Long id) {
		logger.trace(String.format("deleteCampanha(%s)", id));
		
		if(campanhasRepository.existsById(id)) {
			campanhasRepository.deleteById(id);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Método responsável pela validação dos campos de um objeto de campanha.
	 * 
	 * @param campanha
	 * @throws CampanhaInvalidaException
	 */
	private void validarCampanha(Campanha campanha) throws CampanhaInvalidaException {
		if(campanha.getNome() == null || campanha.getNome().isEmpty()) {
			throw new CampanhaInvalidaException(MensagemEnum.MSG_CAMPANHA_NOME_INVALIDO);
		}
		
		if(campanha.getIdTimeCoracao() == null) {
			throw new CampanhaInvalidaException(MensagemEnum.MSG_CAMPANHA_TIME_INVALIDO);
		}
		
		if(campanha.getDataInicio() == null) {
			throw new CampanhaInvalidaException(MensagemEnum.MSG_CAMPANHA_DATA_INICIO_INVALIDA);
		}
		
		if(campanha.getDataTermino() == null) {
			throw new CampanhaInvalidaException(MensagemEnum.MSG_CAMPANHA_DATA_TERMINO_INVALIDA);
		}
		
		if(campanha.getDataInicio().after(campanha.getDataTermino())) {
			throw new CampanhaInvalidaException(MensagemEnum.MSG_CAMPANHA_VIGENCIA_INVALIDA, campanha.getDataInicio(), campanha.getDataTermino());
		}
	}
	
	/**
	 * Método responsável por obter todas as campanhas ativas no período de uma
	 * determinada campanha.
	 * 
	 * @param campanha
	 * @return
	 */
	private List<Campanha> getCampanhasAtivasNoPeriodo(Campanha campanha) {
		logger.trace(String.format("getCampanhasAtivasNoPeriodo(%s)", campanha));
		
		List<CampanhaEntity> campanhasEntity = campanhasRepository
				.findByDataTerminoGreaterThanEqualAndDataTerminoGreaterThanEqualAndDataInicioLessThanEqual(new Date(), campanha.getDataInicio(), campanha.getDataTermino());
		
		return convertToDomain(campanhasEntity);
	}
	
	/**
	 * Método responsável por atualizar as datas de término das campanhas ativas no
	 * período de uma determinada campanha. 
	 * 
	 * @param campanhas
	 * @param dataTermino
	 */
	private void atualizarVigencias(List<Campanha> campanhas, Date dataTermino) {
		logger.trace(String.format("atualizarVigencias(%s, %s)", campanhas, dataTermino));
		
		dataComparacao = new Date(dataTermino.getTime());
		
		campanhas.stream().sorted().forEachOrdered(campanha -> {
			if(DateHelper.addDays(campanha.getDataTermino(), 1).equals(dataComparacao)) {
				dataComparacao = DateHelper.addDays(campanha.getDataTermino(), 2);
				campanha.setDataTermino((Date) dataComparacao.clone());
			} else {
				campanha.setDataTermino(DateHelper.addDays(campanha.getDataTermino(), 1));
			}
			campanha.setVersao(campanha.getVersao() + 1);
		});
		
		campanhasRepository.saveAll(convertToEntity(campanhas));
	}
	
	/**
	 * Método responsável por criar uma nova campanha na base de dados.
	 * @param campanha
	 * @return
	 */
	private Campanha create(Campanha campanha) {
		CampanhaEntity campanhaEntity = convertToEntity(campanha);
		campanhaEntity.setVersao(1);
		campanhaEntity = campanhasRepository.save(campanhaEntity);
		return convertToDomain(campanhaEntity);
	}
	
	/**
	 * Método responsável por atualizar uma campanha na base de dados.
	 * 
	 * @param campanha
	 * @return
	 */
	private Campanha update(Campanha campanha) {
		CampanhaEntity campanhaEntity = convertToEntity(campanha);
		campanhaEntity.setId(campanha.getId());
		campanhaEntity.setVersao(campanha.getVersao() + 1);
		campanhaEntity = campanhasRepository.save(campanhaEntity);
		return convertToDomain(campanhaEntity);
	}
	
	/**
	 * Método responsável pela conversão de uma entidade de campanha
	 * para um objeto POJO de campanha.
	 * 
	 * @param campanhaEntity
	 * @return
	 */
	private Campanha convertToDomain(CampanhaEntity campanhaEntity) {
		if(campanhaEntity == null) return null;
		Campanha campanha = modelMapper.map(campanhaEntity, Campanha.class);
		return campanha;
	}
	
	/**
	 * Método responsável pela conversão de uma lista de entidades de campanha
	 * para uma lista de objetos POJO de campanha
	 * 
	 * @param campanhasEntity
	 * @return
	 */
	private List<Campanha> convertToDomain(List<CampanhaEntity> campanhasEntity) {
		List<Campanha> campanhas = new ArrayList<Campanha>();
		if(campanhasEntity != null && !campanhasEntity.isEmpty()) {
			campanhasEntity.forEach(campanhaEntity -> campanhas.add(convertToDomain(campanhaEntity)));
		}
		return campanhas;
	}
	
	/**
	 * Método responsável pela conversão de um objeto POJO de campanha
	 * para uma entidade de campanha.
	 * 
	 * @param campanha
	 * @return
	 */
	private CampanhaEntity convertToEntity(Campanha campanha) {
		if(campanha == null) return null;
		CampanhaEntity campanhaEntity = modelMapper.map(campanha, CampanhaEntity.class);
		return campanhaEntity;
	}
	
	/**
	 * Método responsável pela conversão de uma lista de objetos POJO de campanha
	 * para uma lista entidades de campanha
	 * 
	 * @param campanhas
	 * @return
	 */
	private List<CampanhaEntity> convertToEntity(List<Campanha> campanhas) {
		List<CampanhaEntity> campanhasEntity = new ArrayList<CampanhaEntity>();
		if(campanhas != null && !campanhas.isEmpty()) {
			campanhas.forEach(campanha -> campanhasEntity.add(convertToEntity(campanha)));
		}
		return campanhasEntity;
	}

}
