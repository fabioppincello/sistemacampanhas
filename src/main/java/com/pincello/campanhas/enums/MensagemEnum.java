package com.pincello.campanhas.enums;

/**
 * Enum utilizado para fornecer as mensagens utilizadas pelo sistema.
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public enum MensagemEnum {
	
	MSG_CAMPANHA_DELETADA("Campanha [%s] deletada."),
	MSG_CAMPANHA_NAO_ENCONTRADA("Campanha [%s] não encontrada."),
	MSG_CAMPANHA_NOME_INVALIDO("O nome da campanha não foi inserido."),
	MSG_CAMPANHA_TIME_INVALIDO("O time da campanha não foi inserido."),
	MSG_CAMPANHA_DATA_INICIO_INVALIDA("A data de início da campanha não foi inserida."),
	MSG_CAMPANHA_DATA_TERMINO_INVALIDA("A data de término da campanha não foi inserida."),
	MSG_CAMPANHA_VIGENCIA_INVALIDA("A data de início da campanha (%s) é maior que a data de término. (%s)");
	
	private String format;
	
	MensagemEnum(String format) {
		this.format = format;
	}
	
	public String getMensagem(Object... args) {
		return String.format(format, args);
	}
	
	public String getMensagem() {
		return format;
	}

}
