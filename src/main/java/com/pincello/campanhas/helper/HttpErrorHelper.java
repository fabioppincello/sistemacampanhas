package com.pincello.campanhas.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.pincello.campanhas.domain.Response;
import com.pincello.campanhas.enums.MensagemEnum;

/**
 * Helper para geração de retornos de erro HTTP
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class HttpErrorHelper {
	
	private static final Logger logger = LogManager.getLogger(HttpErrorHelper.class);
	
	/**
	 * Método responsável por retornar um erro HTTP 500
	 * 
	 * @param e
	 * @return
	 */
	public static<T> ResponseEntity<Response<T>> getErrorResponse(Exception e) {
		logger.error(e);
		Response<T> response = new Response<T>();
		if(e.getMessage() != null) {
			response.getErrors().add(e.getMessage());
		} else {
			response.getErrors().add(e.toString());
		}
		return new ResponseEntity<Response<T>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Método responsável por retornar um erro HTTP 404
	 * 
	 * @param mensagem
	 * @param args
	 * @return
	 */
	public static<T> ResponseEntity<Response<T>> getNotFoundResponse(MensagemEnum mensagem, Object...args) {
		String error = mensagem.getMensagem(args);
		logger.warn(error);
		Response<T> response = new Response<T>();
		response.getErrors().add(error);
		return new ResponseEntity<Response<T>>(response, HttpStatus.NOT_FOUND);
	}

}
