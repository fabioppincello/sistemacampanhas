package com.pincello.campanhas.helper;

import java.util.Calendar;
import java.util.Date;

/**
 * Helper para manipulação de datas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class DateHelper {
	
	/**
	 * Método utilizado para obter uma data como resultado da adição
	 * de x dias à data recebida como argumento
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

}
