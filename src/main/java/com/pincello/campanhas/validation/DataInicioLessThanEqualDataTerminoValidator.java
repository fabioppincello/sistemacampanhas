package com.pincello.campanhas.validation;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * Implementação da validação adicional para não permitir que a data de início
 * da campanha seja maior que a data de término
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class DataInicioLessThanEqualDataTerminoValidator implements ConstraintValidator<DataInicioLessThanEqualDataTermino, Object> {

	private String dataInicio;
	private String dataTermino;
	
	@Override
    public void initialize(DataInicioLessThanEqualDataTermino annotation) {
        dataInicio = annotation.dataInicio();
        dataTermino = annotation.dataTermino();
    }
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value == null) {
            return true;
        }
		
		BeanWrapper wrapper = new BeanWrapperImpl(value);
		Date dataInicio = (Date) wrapper.getPropertyValue(this.dataInicio);
		Date dataTermino = (Date) wrapper.getPropertyValue(this.dataTermino);
		
		if(dataInicio == null || dataTermino == null) {
			return true;
		}
		
		if(dataInicio.compareTo(dataTermino) > 0) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                .addPropertyNode(this.dataTermino)
                .addConstraintViolation();
                return false;
		}
		
		return true;
	}

}
