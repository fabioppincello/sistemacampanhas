package com.pincello.campanhas.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validação adicional para não permitir que a data de início
 * da campanha seja maior que a data de término
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DataInicioLessThanEqualDataTerminoValidator.class)
@Documented
public @interface DataInicioLessThanEqualDataTermino {
	
	String dataInicio();
	String dataTermino();
	
	String message() default "{DataInicioLessThanEqualDataTerminoValidator.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
