package com.pincello.campanhas.domain;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pincello.campanhas.validation.DataInicioLessThanEqualDataTermino;

/**
 * Classe utilizada para modelagem de uma campanha
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@DataInicioLessThanEqualDataTermino(
		dataInicio = "dataInicio",
		dataTermino = "dataTermino"
)
public class Campanha implements Serializable, Comparable<Campanha> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2518058111476188551L;
	
	private Long id;
	
	@NotNull @NotBlank
	private String nome;
	
	@NotNull
	private Integer idTimeCoracao;
	
	@NotNull @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy z")
	private Date dataInicio;
	
	@NotNull @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy z")
	private Date dataTermino;
	
	private Integer versao;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getIdTimeCoracao() {
		return idTimeCoracao;
	}
	
	public void setIdTimeCoracao(Integer idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}
	
	public Date getDataInicio() {
		return dataInicio;
	}
	
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public Date getDataTermino() {
		return dataTermino;
	}
	
	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
	
	public Integer getVersao() {
		return versao;
	}
	
	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	@Override
	public int compareTo(Campanha o) {
		if(dataTermino == null || o.dataTermino == null || dataTermino.equals(o.dataTermino)) {
			if(dataInicio == null || o.dataInicio == null || dataInicio.equals(dataTermino)) {
				return versao.compareTo(o.versao);
			}
			return dataInicio.compareTo(o.dataInicio);
		}
		return dataTermino.compareTo(o.dataTermino);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
			.append("{ id: ")
			.append(id)
			.append(", nome: \"")
			.append(nome)
			.append("\", idTimeCoracao: ")
			.append(idTimeCoracao)
			.append(", dataInicio: \"")
			.append(dataInicio)
			.append("\", dataTermino: \"")
			.append(dataTermino)
			.append("\" }");
		
		return builder.toString();
	}

}
