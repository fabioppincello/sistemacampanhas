package com.pincello.campanhas.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Classe utilizada para modelagem de um objeto genérico para resposta das APIs
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@JsonInclude(Include.NON_NULL)
public class Response<T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 263574249353031574L;
	
	private T data;
	private List<String> errors;
	
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}

	public List<String> getErrors() {
		if(errors == null) {
			errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public void setErrors(BindingResult result) {
		for(ObjectError objectError : result.getAllErrors()) {
			getErrors().add(objectError.toString());
		}
	}

}
