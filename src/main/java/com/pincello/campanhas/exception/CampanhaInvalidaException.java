package com.pincello.campanhas.exception;

import com.pincello.campanhas.enums.MensagemEnum;

/**
 * Exceção utilizada para informar que um objeto de campanha está em um formato inválido
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class CampanhaInvalidaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7234166863917282663L;
	
	public CampanhaInvalidaException(String message) {
		super(message);
	}
	
	public CampanhaInvalidaException(MensagemEnum mensagemEnum) {
		super(mensagemEnum.getMensagem());
	}
	
	public CampanhaInvalidaException(MensagemEnum mensagemEnum, Object...args) {
		super(mensagemEnum.getMensagem(args));
	}

}
