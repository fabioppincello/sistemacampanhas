package com.pincello.campanhas.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidade para a tabela de campanhas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Entity
@Table(name = "campanha")
public class CampanhaEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3666145575985202328L;

	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	private Integer idTimeCoracao;
	@Temporal(TemporalType.DATE)
	private Date dataInicio;
	@Temporal(TemporalType.DATE)
	private Date dataTermino;
	private Integer versao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdTimeCoracao() {
		return idTimeCoracao;
	}

	public void setIdTimeCoracao(Integer idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}
	
	public Integer getVersao() {
		return versao;
	}
	
	public void setVersao(Integer versao) {
		this.versao = versao;
	}

}
