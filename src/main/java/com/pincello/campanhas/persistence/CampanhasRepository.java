package com.pincello.campanhas.persistence;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pincello.campanhas.persistence.entity.CampanhaEntity;

/**
 * Repository de campanhas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public interface CampanhasRepository extends JpaRepository<CampanhaEntity, Long> {
	
	/**
	 * Obtém da base de dados apenas as campanhas com data de término
	 * maior ou igual à data especificada.
	 * 
	 * @param data
	 * @return
	 */
	List<CampanhaEntity> findByDataTerminoGreaterThanEqual(Date data);
	
	/**
	 * Obtém da base de dados apenas a campanha com id especificado, desde
	 * que a data de término seja maior ou igual à data especificada.
	 * 
	 * @param id
	 * @param data
	 * @return
	 */
	CampanhaEntity findByIdAndDataTerminoGreaterThanEqual(Long id, Date data);
	
	/**
	 * Obtém da base de dados apenas as campanhas com vigência ativa para o
	 * período especificado.
	 * 
	 * @param data
	 * @param dataInicio
	 * @param dataTermino
	 * @return
	 */
	List<CampanhaEntity> findByDataTerminoGreaterThanEqualAndDataTerminoGreaterThanEqualAndDataInicioLessThanEqual(Date data, Date dataInicio, Date dataTermino);
	
	/**
	 * Obtém da base de dados apenas as campanhas com idTimeCoracao especificado, desde
	 * que a data de término seja maior ou igual à data especificada.
	 * 
	 * @param idTimeCoracao
	 * @param data
	 * @return
	 */
	List<CampanhaEntity> findByIdTimeCoracaoAndDataTerminoGreaterThanEqual(Integer idTimeCoracao, Date data);

}
