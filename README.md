## Sistema de Campanhas

A aplicação foi criada a partir de um novo Projeto **Spring Boot**, que fornece de forma fácil uma configuração inicial para um projeto Spring.
O projeto foi configurado para utilizar o **Apache Maven** como ferramenta de automação de compilação e gerenciamento de dependências, a versão **8** do Java, o banco de dados em memória **H2** como solução integrada para persistência dos dados, o framework **Spring Data JPA** para a criação de repositórios JPA no Java, o framework **Log4j** para logging e o framework **Spring MVC** para exposição das APIs REST.
Para organizar o projeto, foi construída uma arquitetura em camadas, separando-as em 3 partes:

* camada Web: classe Controller, responsável pela exposição das APIs, localizada na package **com.pincello.campanhas.controller**. Para a exposição das APIs, foram utilizadas as annotations fornecidas pelo Spring MVC.
* camada de negócio: classe Service, responsável pela implementação das regras de negócio, localizada na package **com.pincello.campanhas.service**.
* camada de dados: classes / interfaces responsáveis pela persistência das entidades na base de dados. Essa camada possui as entidades que mapeiam as tabelas do banco de dados, localizadas na package **com.pincello.campanhas.persistence.entity**, e o repositório JPA, localizado na package **com.pincello.campanhas.persistence**, para a utilização dos métodos que realizam operações na base de dados.

Foram implementados testes unitários das camadas Web e de negócio utilizando **JUnit**. Para os testes das chamadas das APIs, foi utilizado o **MockMVC**.

OBS: A aplicação roda na porta 8080.